package com.example.auliaheryanov.kukulinerver3.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.auliaheryanov.kukulinerver3.R;
import com.example.auliaheryanov.kukulinerver3.model.userTab1;

import java.util.List;

public class userAdapter1 extends RecyclerView.Adapter<userAdapter1.myHolder> {
    private Context context;
    List<userTab1> data;


    public userAdapter1(Context context, List<userTab1>data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public myHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layout = LayoutInflater.from(context).inflate(R.layout.user_adapter_grid_layout,parent,false);

        myHolder holder = new myHolder(layout);
        return holder;
    }

    @Override
    public void onBindViewHolder(myHolder holder, int position) {
        holder.gambar.setImageResource(data.get(position).getImage());
        holder.namaToko.setText(data.get(position).getNamaToko());
        holder.JamBuka.setText(data.get(position).getJamBuka());
        holder.rating.setRating(data.get(position).getRatting());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class myHolder extends RecyclerView.ViewHolder{
        ImageView gambar;
        TextView namaToko,JamBuka;
        RatingBar rating;
        public myHolder(View itemView) {
            super(itemView);
            gambar = itemView.findViewById(R.id.gridview_adapter_layout_image);
            namaToko   = itemView.findViewById(R.id.gridview_adapter_layout_text);
            rating = itemView.findViewById(R.id.rating_newest_store);
            JamBuka = itemView.findViewById(R.id.jam_buka_newest_store);
        }
    }
}
