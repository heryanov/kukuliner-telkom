package com.example.auliaheryanov.kukulinerver3.model;

public class userProgress {
    private int image;
    private String namaToko, jamBuka,progress;
    private float ratting;

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public userProgress(String namaToko, int image, String jamBuka, int ratting, String progress) {
        this.image = image;

        this.namaToko = namaToko;
        this.jamBuka = jamBuka;
        this.ratting = ratting;
        this.progress = progress;
    }

    public float getRatting() {
        return ratting;
    }

    public void setRatting(float ratting) {
        this.ratting = ratting;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getNamaToko() {
        return namaToko;
    }

    public void setNamaToko(String namaToko) {
        this.namaToko = namaToko;
    }

    public String getJamBuka() {
        return jamBuka;
    }

    public void setJamBuka(String jamBuka) {
        this.jamBuka = jamBuka;
    }
}
