package com.example.auliaheryanov.kukulinerver3.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.example.auliaheryanov.kukulinerver3.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class contentFragment3 extends Fragment implements OnMapReadyCallback {
    GoogleMap googleMap;
    MapView mapView;
    FrameLayout frameLayout;
    View rootView;
    public contentFragment3() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.user_content3,container,false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapView = rootView.findViewById(R.id.maps);
        if(mapView != null){
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(getContext());
        LatLng munjulkoor = new LatLng(-6.976032, 107.633386);
        googleMap.addMarker(new MarkerOptions().position(munjulkoor).title("MUNJUL FOREVER"));
        CameraPosition munjul = CameraPosition.builder().target(munjulkoor).zoom(16).bearing(90).tilt(30).build();
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(munjulkoor));
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(munjul));

    }
}
