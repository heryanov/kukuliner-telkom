package com.example.auliaheryanov.kukulinerver3.model;

public class penjualTab2 {
    private int image;
    private String namaToko, pesanan,progress;
    private int harga;

    public penjualTab2(String namaToko, int image, String pesanan, int harga,String progress) {
        this.image = image;
        this.namaToko = namaToko;
        this.pesanan = pesanan;
        this.harga = harga;
        this.progress = progress;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getNamaToko() {
        return namaToko;
    }

    public void setNamaToko(String namaToko) {
        this.namaToko = namaToko;
    }

    public String getPesanan() {
        return pesanan;
    }

    public void setPesanan(String pesanan) {
        this.pesanan = pesanan;
    }

    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }
}

