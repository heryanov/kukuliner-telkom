package com.example.auliaheryanov.kukulinerver3.Model;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Penjual {

    public String name;
    public String password;
    public String email;
    public String Alamat;
    public String noHP;
    public String tipe;
    public String aktivasi;

    // Default constructor required for calls to
    // DataSnapshot.getValue(User.class)
    public Penjual() {
    }

    public Penjual(String name, String email, String password, String Alamat, String noHP, String tipe, String aktivasi) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.Alamat = Alamat;
        this.noHP = noHP;
        this.tipe = tipe;
        this.aktivasi = aktivasi;
    }

}