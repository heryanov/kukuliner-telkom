package com.example.auliaheryanov.kukulinerver3.model;

public class userTab1 {
    private int image;
    private String namaToko, jamBuka;
    private float ratting;

    public userTab1(String namaToko, int image,  String jamBuka, int ratting) {
        this.image = image;
        this.namaToko = namaToko;
        this.jamBuka = jamBuka;
        this.ratting = ratting;
    }

    public float getRatting() {
        return ratting;
    }

    public void setRatting(int ratting) {
        this.ratting = ratting;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getNamaToko() {
        return namaToko;
    }

    public void setNamaToko(String namaToko) {
        this.namaToko = namaToko;
    }

    public String getJamBuka() {
        return jamBuka;
    }

    public void setJamBuka(String jamBuka) {
        this.jamBuka = jamBuka;
    }
}
