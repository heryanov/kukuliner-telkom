package com.example.auliaheryanov.kukulinerver3.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.auliaheryanov.kukulinerver3.R;
import com.example.auliaheryanov.kukulinerver3.adapter.userAdapter1;
import com.example.auliaheryanov.kukulinerver3.model.userTab1;

import java.util.ArrayList;
import java.util.List;

public class contentFragment extends Fragment {
    private RecyclerView recyclerView;
    GridLayoutManager layoutManager;
    private List<userTab1> data;

    String namaToko, jam;
    int ratting, img;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.user_content, container, false);
        recyclerView = rootView.findViewById(R.id.recycleview_content_user);
        layoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        userAdapter1 adapter1 = new userAdapter1(getContext(), data);
        recyclerView.setAdapter(adapter1);
        return rootView;


    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
}