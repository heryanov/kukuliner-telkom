package com.example.auliaheryanov.kukulinerver3.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.auliaheryanov.kukulinerver3.R;
import com.example.auliaheryanov.kukulinerver3.adapter.userAdapter1;
import com.example.auliaheryanov.kukulinerver3.model.userTab1;

import java.util.ArrayList;
import java.util.List;

public class contentFragment2 extends Fragment {
    private RecyclerView recyclerView;
    GridLayoutManager layoutManager;
    private List<userTab1> data;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.user_content2, container,    false);
        recyclerView  = rootView.findViewById(R.id.recycleview_content_user2);
        layoutManager = new GridLayoutManager(getActivity(),3);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        userAdapter1 adapter1 = new userAdapter1(getContext(),data);
        recyclerView.setAdapter(adapter1);
        return rootView;


    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        data = new ArrayList<>();
        data.add(new userTab1("store 1",R.drawable.tumpeng,"Buka 06.00 - 18.00",1));
        data.add(new userTab1("store 2",R.drawable.tumpeng,"Buka 07.00 - 21.00",2));
        data.add(new userTab1("store 3",R.drawable.tumpeng,"Buka 08.00 - 20.00",3));
        data.add(new userTab1("store 4",R.drawable.tumpeng,"Buka 07.00 - 16.00",1));
        data.add(new userTab1("store 5",R.drawable.tumpeng,"Buka 08.00 - 18.00",2));
        data.add(new userTab1("store 6",R.drawable.tumpeng,"Buka 09.00 - 21.00",3));
    }
}
