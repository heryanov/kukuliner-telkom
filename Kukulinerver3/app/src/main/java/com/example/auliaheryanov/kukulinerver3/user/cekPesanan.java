package com.example.auliaheryanov.kukulinerver3.user;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.auliaheryanov.kukulinerver3.R;
import com.example.auliaheryanov.kukulinerver3.adapter.userAdapter2;
import com.example.auliaheryanov.kukulinerver3.model.userProgress;

import java.util.ArrayList;
import java.util.List;

public class cekPesanan extends Fragment {
    private RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    private List<userProgress> data;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.user_cek_pesanan, container,    false);
        recyclerView  = rootView.findViewById(R.id.recycleview_cek_pesanan_user);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        userAdapter2 adapter2 = new userAdapter2(getContext(),data);
        recyclerView.setAdapter(adapter2);
        return rootView;


    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        data = new ArrayList<>();
        data.add(new userProgress("munjul", R.drawable.tumpeng,"Buka 08.00 - 22.00",5,"onProgress"));
        data.add(new userProgress("munjul",R.drawable.tumpeng,"Buka 08.00 - 22.00",5,"onProgress"));
        data.add(new userProgress("munjul",R.drawable.tumpeng,"Buka 08.00 - 22.00",5,"onProgress"));
        data.add(new userProgress("munjul",R.drawable.tumpeng,"Buka 08.00 - 22.00",5,"onProgress"));
        data.add(new userProgress("munjul",R.drawable.tumpeng,"Buka 08.00 - 22.00",5,"onProgress"));
        data.add(new userProgress("munjul",R.drawable.tumpeng,"Buka 08.00 - 22.00",5,"onProgress"));

    }
}
