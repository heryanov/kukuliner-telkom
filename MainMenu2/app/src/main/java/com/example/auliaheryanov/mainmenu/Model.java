package com.example.auliaheryanov.mainmenu;

/**
 * Created by Asus on 3/23/2018.
 */

public class Model {
    private String nama, deskripsi;
    private int gambar;

    public Model(String nama, String deskripsi, int gambar) {
        this.nama = nama;
        this.deskripsi = deskripsi;
        this.gambar = gambar;

    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public int getGambar() {
        return gambar;
    }

    public void setGambar(int gambar) {
        this.gambar = gambar;
    }
}

