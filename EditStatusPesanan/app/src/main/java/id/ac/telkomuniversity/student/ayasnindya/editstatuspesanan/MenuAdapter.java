package id.ac.telkomuniversity.student.ayasnindya.editstatuspesanan;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

/**
 * Created by Hewlett Packard unyu on 21 Mar 2018.
 */

public class MenuAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {
    //deklarasi variable context

    private final Context context;

    String[] name = {"Nasi Goreng Special", "Mie Goreng Special", "Nasi Kuning", "Nasi Wagyu",
            "Es Teh Manis", "Pancake Coklat", "Ice Cream"};
    int [] gambar = {R.drawable.nasi_goreng, R.drawable.nasi_kuning, R.drawable.nasi_wagyu, R.drawable.mie_goreng, R.drawable.pancake,
            R.drawable.ice_cream, R.drawable.es_teh, R.drawable.ice_cream};

    LayoutInflater inflater;

    public MenuAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.item_list, parent, false);

        RecyclerViewHolder viewHolder = new RecyclerViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {

        holder.tv1.setText(name[position]);
        holder.tv1.setOnClickListener(clickListener);
        holder.imageView.setImageResource(gambar[position]);
        holder.imageView.setOnClickListener(clickListener);
        holder.tv1.setTag(holder);
        holder.imageView.setTag(holder);

    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
//member aksi saat cardview diklik berdasarkan posisi tertentu
            RecyclerViewHolder vholder = (RecyclerViewHolder) v.getTag();

            int position = vholder.getPosition();


            Toast.makeText(context, "Menu ini berada di posisi " + position, Toast.LENGTH_LONG).show();


        }
    };


    @Override
    public int getItemCount() {
        return name.length;
    }
}