package com.example.kukuliner.kuliner.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.kukuliner.kuliner.Model.userTab1;
import com.example.kukuliner.kuliner.Model.userTab2;
import com.example.kukuliner.kuliner.R;
import com.example.kukuliner.kuliner.adapter.userAdapter1;
import com.example.kukuliner.kuliner.adapter.userAdapter3;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by candra on 4/8/2018.
 */
public class contentFragment2 extends Fragment {
    private RecyclerView recyclerView;
    GridLayoutManager layoutManager;
    private List<userTab2> data;
    private DatabaseReference mDatabase;
    private StorageReference mStorageRef;
    userAdapter3 adapter1;
    String namaToko,jam,username;
    int ratting;
    StorageReference img;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.user_content_2, container,    false);
        recyclerView  = rootView.findViewById(R.id.recycleview_content_user2);
        layoutManager = new GridLayoutManager(getActivity(),3);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        adapter1 = new userAdapter3(getContext(),data);
        recyclerView.setAdapter(adapter1);
        mStorageRef = FirebaseStorage.getInstance().getReference().child("Profile");
        return rootView;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mDatabase = FirebaseDatabase.getInstance().getReference().child("penjual");
        data = new ArrayList<>();
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String status;
                for(DataSnapshot ds:dataSnapshot.getChildren()){
                    namaToko = ds.child("name").getValue().toString();
                    jam = ds.child("Alamat").getValue().toString();
                    username = ds.child("name").getValue().toString();
                    img = mStorageRef.child(username);
                    status = ds.child("aktivasi").getValue().toString();
                    ratting = Integer.valueOf(ds.child("ratting").getValue().toString()) ;
                    Log.d("cek contentFrament","data = "+img.toString());
                    if(ratting >=4 && status.equals("yes")){
                        data.add(new userTab2(namaToko,img,jam,ratting,username));
                    }
                }
                adapter1.notifyDataSetChanged();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }
}