package com.example.kukuliner.kuliner.Model;

/**
 * Created by candra on 4/22/2018.
 */
public class feedback1 {
    private String text;
    private String username;

    public feedback1(String text, String username) {
        this.text = text;
        this.username = username;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}