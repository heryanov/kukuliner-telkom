package com.example.auliaheryanov.kukulinerver1.MainMenu;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.auliaheryanov.kukulinerver1.R;

/**
 * Created by Asus on 3/23/2018.
 */

public class MenuAdapter extends RecyclerView.Adapter<RecyclerViewHolder>{

    private final Context context;

    String[] name = {"Warung Biru", "Warung Merah", "Warung Nasi", "Juanda",
            "Warung Pojok", "Sederhana", "Warung Tumpeng", "Warung Bali"};
    int [] gambar = {R.drawable.a, R.drawable.nasi, R.drawable.warung, R.drawable.juanda, R.drawable.pojok, R.drawable.sederhana, R.drawable.tumpeng, R.drawable.warungbali};

    LayoutInflater inflater;

    public MenuAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.item_list, parent, false);

        RecyclerViewHolder viewHolder = new RecyclerViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        holder.tv1.setText(name[position]);
        holder.tv1.setOnClickListener(clickListener);
        holder.imageView.setImageResource(gambar[position]);
        holder.imageView.setOnClickListener(clickListener);
        holder.tv1.setTag(holder);
        holder.imageView.setTag(holder);
    }
    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
//member aksi saat cardview diklik berdasarkan posisi tertentu
            RecyclerViewHolder vholder = (RecyclerViewHolder) v.getTag();

            int position = vholder.getPosition();


            Toast.makeText(context, "Menu ini berada di posisi " + position, Toast.LENGTH_LONG).show();


        }
    };
    @Override
    public int getItemCount() {
        return name.length;
    }
}





