package com.example.ayupriyambodo.kukuliner;

import android.content.Context;
import android.provider.ContactsContract;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

/**
 * Created by AYUPRIYAMBODO on 19/02/2018.
 */

public class AdapterMakanan extends BaseAdapter {
    Context context;

    public AdapterMakanan (Context c){
        context = c;
    }

    public int[] menu = {
            R.drawable.aqua, R.drawable.es_jeruk, R.drawable.es_teh,
            R.drawable.kuetiauw_goreng, R.drawable.mie_goreng, R.drawable.nasi_ayam_goreng,
            R.drawable.nasi_goreng
    };

    @Override
    public int getCount() {
        return menu.length;
    }

    @Override
    public Object getItem(int position) {
        return menu[position];
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ImageView im = new ImageView(context);
        im.setImageResource(menu[position]);
        im.setScaleType(ImageView.ScaleType.CENTER_CROP);
        im.setLayoutParams(new GridView.LayoutParams(250, 250));
        return im;
    }
}
