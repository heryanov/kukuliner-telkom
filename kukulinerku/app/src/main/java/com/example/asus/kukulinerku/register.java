package com.example.asus.kukulinerku;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

public class register extends AppCompatActivity {
    Button daftar;
    EditText nama, username, password, email, alamat, noidenditas, tanggalLahir, noTelfon, pertanyaan, jawaban;
    String gender;
    Boolean checker = false;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        daftar = findViewById(R.id.signUp_button);
        nama = findViewById(R.id.nama_register);
        username = findViewById(R.id.username_register);
        password = findViewById(R.id.register_password);
        email = findViewById(R.id.email_register);
        alamat = findViewById(R.id.noidentitas_register);
        noidenditas = findViewById(R.id.noidentitas_register);
        tanggalLahir = findViewById(R.id.tanggal_register);
        noTelfon = findViewById(R.id.notelepon_register);
        pertanyaan = findViewById(R.id.pertanyaan_register);
        jawaban = findViewById(R.id.jawaban_register);
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.radio_male:
                if (checked)
                    gender = "male";
                break;
            case R.id.radio_female:
                if (checked)
                    gender = "female";
                break;
        }
    }
}
